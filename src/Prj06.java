public class Prj06 {
    public static void main(String[] args) {
        Processor[] processors = new Processor[4];

        processors[0] = new Addition();
        processors[1] = new Subtraction();
        processors[2] = new Multiplication();
        processors[3] = new Division();

        for(Processor processor : processors)
            processor.processs(10,5);


    }
}
